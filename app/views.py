from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.models import User, UserManager
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.generic import UpdateView
import  ast
from .models import Persona, Recurso
from django.db import connection, connections
from django.utils import timezone
from .models import *
from django.contrib.auth.models import User, UserManager
from .models import Persona, SignosVitales, Paciente,Medico,ConsultaMedica,Diagnosticos,Tratamientos,Catalogo,TipoCatalogo\
    ,Cirugias,ProgramacionCirugia,Resultados,GrupoRecurso
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import  login_required
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import CitaForm, AdjuntoForm
import  _md5


# Create your views here.
@login_required()
def index(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    template = loader.get_template('app/index.html')

    context = {
        'user': request.user,
        'menu':menu,
    }
    return HttpResponse(template.render(context, request))

@method_decorator(login_required, name='dispatch')
class PersonasView(generic.ListView):
    template_name = 'app/persona/list.html'
    context_object_name = 'personas'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # menu
        perUser = Persona.objects.get(user=self.request.user)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
            row = cursor.fetchone()
        menu = Recurso.objects.filter(
            id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        # menu
        context={'menu':menu}
        return context
    def get_queryset(self):
        return None

    def post(self, request, *args, **kwargs):
        noExpediente = request.POST['noExpediente']
        nombre =request.POST['nombrePaciente']
        apellido =request.POST['apellidoPaciente']
        personas=None
        # menu
        perUser = Persona.objects.get(user=request.user)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
            row = cursor.fetchone()
        menu = Recurso.objects.filter(
            id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        # menu
        if noExpediente!='':
            personas = Paciente.objects.filter(codigo_expediente=noExpediente).all()
        elif nombre!='' or apellido!='':
            personas = Paciente.objects.filter(id_persona__nombre__icontains=nombre,
                                               id_persona__apellido__icontains=apellido).all()
        else:
            return render(request, self.template_name, {'error_message': 'Debe usar por lo menos un filtro','menu':menu})

        return render(request, self.template_name, {'personas': personas,'menu':menu})

@method_decorator(login_required, name='dispatch')
class CitasView(generic.ListView):

    template_name = 'app/cita/list.html'
    context_object_name = 'personas'
    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        # menu
        perUser = Persona.objects.get(user=self.request.user)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
            row = cursor.fetchone()
        menu = Recurso.objects.filter(
            id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        # menu
        context = {'menu': menu}
        return context
    def get_queryset(self):
        return None

    def post(self, request, *args, **kwargs):
        noExpediente = request.POST['noExpediente']
        nombre =request.POST['nombrePaciente']
        apellido =request.POST['apellidoPaciente']
        personas=None
        #menu
        perUser = Persona.objects.get(user=request.user)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
            row = cursor.fetchone()
        menu = Recurso.objects.filter(
            id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        #menu
        if noExpediente!='':
            personas = Paciente.objects.filter(codigo_expediente=noExpediente).all()
        elif nombre!='' or apellido!='':
            personas = Paciente.objects.filter(id_persona__nombre__icontains=nombre,
                                               id_persona__apellido__icontains=apellido).all()
        else:
            return render(request, self.template_name, {'error_message': 'Debe usar por lo menos un filtro','menu':menu})

        return render(request, self.template_name, {'personas': personas,'menu':menu})

def citas_create(request):
        data = dict()

        if request.method == 'POST':
            form = CitaForm(request.POST)
            if form.is_valid():
                form.save()
                data['form_is_valid'] = True
            else:
                data['form_is_valid'] = False
        else:
            form = CitaForm()

        context = {'form': form}
        data['html_form'] = render_to_string('app/Cita/form.html',
                                             context,
                                             request=request
                                             )
        return JsonResponse(data)

def citas_paciente(request):
    data =dict()
    id_paciente=request.GET['id_paciente']
    citas=Cita.objects.filter(id_paciente=id_paciente,fecha__gte=timezone.now()).all();
    context={'citas':citas}
    data['html_form'] = render_to_string('app/Cita/view.html',context=context,request=request)
    return JsonResponse(data)




def UsuarioCreate(request):
    if request.method =='POST':
        username =request.POST['usuario']
        password=request.POST['clave']
        idPersona=request.POST['idPersona']
        persona=Persona.objects.get(id_persona=idPersona)
        nuevoUsuario= User(username=username)
        nuevoUsuario.set_password(password)
        nuevoUsuario.email=persona.email
        nuevoUsuario.save()

        persona.user_id=User.objects.get(id=nuevoUsuario.id)
        persona.save()

    return redirect('/app/personas/')

#aris
@login_required()
def consulta(request):
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    try:
        if request.method=='POST':
            pk = request.POST['npaciente']
            nombre = request.POST['nompaciente']
            apellido = request.POST['apepaciente']
            if pk=='' and nombre=='' and apellido=='':
                return render(request, 'app/ConsultaMedica/registroConsulta.html', {
                    'error_message': "No Hay Datos que Correspondan", 'menu': menu})
            else:
                hola=''
            if pk=='':
                per=Paciente.objects.get(id_persona=Persona.objects.get(nombre=nombre,apellido=apellido))
                pk=per.codigo_expediente
            else:
                hola=''
            fecha1 = timezone.now()
            try:
                examen=Examen.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=pk),exa_realizado='s',exa_revisado='n')
                adjunto=Adjuntos.objects.filter(id_examen__in=Examen.objects.filter(id_paciente=
                                                                                Paciente.objects.get(codigo_expediente=pk),
                                                                                exa_realizado='s',exa_revisado='n').values_list('id_examen'))


            except(KeyError,Paciente.DoesNotExist,Examen.DoesNotExist):
                examen=''
            try:
                resultado = Resultados.objects.filter(
                    id_examen__in=Examen.objects.filter(exa_realizado='s',exa_revisado='n',
                                                                                     id_paciente=Paciente.objects.get(
                                                                                         codigo_expediente=pk)))

            except(KeyError,Resultados.DoesNotExist,Examen.DoesNotExist):
                resultado=''



            if pk!='':
                modelo1 = Paciente.objects.get(codigo_expediente=pk)
            elif nombre!='' and apellido!='':
                modelo1 = Paciente.objects.get(id_persona=Persona.objects.get(nombre=nombre,apellido=apellido))

            else:
                modelo1=Paciente.objects.get(codigo_expediente=0)

            modelo22 = SignosVitales.objects.filter(id_paciente=modelo1.id_paciente,med='n',fecha=fecha1).order_by('-id_signovital2')[:1]

            perUser = Persona.objects.get(user=request.user)
            modelo3 = Medico.objects.get(id_persona=perUser)
            fecha=fecha1.date().isoformat()
            catalogo=Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=1))
            catalogoDiag=Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=3))
            context = {'paciente': modelo1, 'signovital': modelo22, 'persona': modelo3, 'fecha':fecha,'catalogo':catalogo,'catalogoDiag':catalogoDiag,'menu':menu,'examen':examen,'resultado':resultado}
            return render(request, "app/ConsultaMedica/registroConsulta.html", context)
        else:
            context = {'menu':menu}
            return render(request,'app/ConsultaMedica/registroConsulta.html',context)

    except(KeyError, Paciente.DoesNotExist,SignosVitales.DoesNotExist,Medico.DoesNotExist,TipoCatalogo.DoesNotExist):

        return render(request, 'app/ConsultaMedica/registroConsulta.html', {
                'error_message': "No Hay Datos que Correspondan",'menu':menu})



@login_required()
def guardarconsulta(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    idpaciente=request.POST['personaid']
    descripcion=request.POST['descripcion']
    idmedico=request.POST['nmed']
    fecha=request.POST['fecha']
    idsignovital=request.POST['signov']
    diagnostico=request.POST['diagnostico']
    cirugia=request.POST['tcirugia']
    fechacirugia=request.POST['fechaC']
    hora=request.POST['hora']
    catalogodia=request.POST['catalogoDia']
    medico = Medico(id_medico=idmedico)
    signovital=SignosVitales(id_signovital2=idsignovital)
    paciente=Paciente(id_paciente=idpaciente)
    consulta=ConsultaMedica(fecha_consulta=fecha, descripcion=descripcion, id_medico=medico, id_signovital2=signovital,id_paciente=paciente)
    consulta.save()

    with connection.cursor() as cursor:
        cursor.callproc('insertarDiagnostico', [consulta.id_conultamedica, diagnostico,consulta.fecha_consulta,catalogodia])
        cursor.callproc('vinculadoMed',[idpaciente,fecha])
        cursor.callproc('ExamenesRevisados', [idpaciente])

        if cirugia != '0':
          cursor.callproc('ProgramarCirugia', [cirugia,consulta.id_conultamedica, fechacirugia,hora])
    context = {'menu':menu}
    return render(request,'app/ConsultaMedica/registroConsulta.html',context)

@login_required()
def listarConsulta(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
       if request.method=='POST':

          perUser = Persona.objects.get(user=request.user)
          medico=Medico.objects.get(id_persona=perUser)
          expediente=request.POST['epaciente']
          fecha = request.POST['fecha']
          diagnostico=request.POST['diagnostico']
          if diagnostico!='':
             catalogo=Catalogo.objects.get(codigo=diagnostico)
          else:
              catalogo=''
          if expediente != '' and diagnostico != '' and fecha!='':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta1 = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico,fecha_consulta=fecha)
              consulta=consulta1.filter(id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica')).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html', {'consulta': consulta, 'menu': menu})

          if expediente != '' and fecha != '':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico,fecha_consulta=fecha).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html',{'consulta': consulta,  'menu': menu})

          if expediente != '' and diagnostico !='':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta1 = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico)
              consulta = consulta1.filter(id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo)).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html', {'consulta': consulta, 'menu': menu})
          if diagnostico != '' and fecha!='':
              consulta = ConsultaMedica.objects.filter(id_medico=medico.id_medico,id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica'),fecha_consulta=fecha).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html', {'consulta': consulta, 'menu': menu})

          if expediente!='':
              paciente=Paciente.objects.get(codigo_expediente=expediente)
              consulta=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html',{'consulta':consulta,'menu':menu})
          if diagnostico != '':
              consulta = ConsultaMedica.objects.filter(id_medico=medico.id_medico,id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica')).order_by('-fecha_consulta','-id_conultamedica')
              return render(request, 'app/ConsultaMedica/buscarConsulta.html', {'consulta': consulta, 'menu': menu})
          elif fecha!='':
             consulta= ConsultaMedica.objects.filter(fecha_consulta=fecha,id_medico=medico.id_medico).order_by('-fecha_consulta','-id_conultamedica')
             return render(request, 'app/ConsultaMedica/buscarConsulta.html', {'consulta': consulta, 'menu': menu})
          else:
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico)
              return render(request, 'app/ConsultaMedica/buscarConsulta.html',{'consulta': consulta, 'menu': menu})



       else:
          template = loader.get_template('app/ConsultaMedica/buscarConsulta.html')
          context = {'menu':menu}
          return HttpResponse(template.render(context, request))
    except(KeyError, Paciente.DoesNotExist, ConsultaMedica.DoesNotExist):

     return render(request, 'app/ConsultaMedica/buscarConsulta.html', {
        'error_message': "No Existen Registros del Paciente",'menu':menu})
@login_required()
def detalleConsulta(request, id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    consulta=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
    diagnostico=Diagnosticos.objects.filter(id_conultamedica=consulta.id_conultamedica)
    tratamiento=Tratamientos.objects.filter(id_conultamedica=consulta.id_conultamedica)
    cirugia=ProgramacionCirugia.objects.filter(id_conultamedica=id_conultamedica)
    examen=Examen.objects.filter(id_conultamedica=consulta.id_conultamedica)
    return render(request,'app/ConsultaMedica/verConsulta.html',{'consulta':consulta,'diagnostico':diagnostico,
                                                                 'tratamiento':tratamiento,'cirugia':cirugia,'menu':menu,'examen':examen})
@login_required()
def editarConsulta(request, id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        descripcion=request.POST['descripcion']
        fecha=request.POST['fecha']
        iddiagnostico=request.POST['iddiagnostico']
        descripciondiag=request.POST['diagnostico']
        diagnostico=Diagnosticos.objects.get(id_diagnostico2=iddiagnostico)
        consulta=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        consulta.descripcion=descripcion
        consulta.fecha_consulta=fecha
        consulta.save()
        diagnostico.descripcion=descripciondiag
        diagnostico.fecha_servicio=fecha
        diagnostico.id_catalogo=Catalogo.objects.get(codigo=request.POST['catalogoDia'])
        diagnostico.save()
        perUser = Persona.objects.get(user=request.user)
        medico=Medico.objects.get(id_persona=perUser)
        consulta2=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        catalogoDiag = Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=3))
        mensaje = 'Actualizado Correctamente'
        return render(request,'app/ConsultaMedica/editarConsulta.html',{'consulta':consulta2,'medico':medico,'diagnostico':diagnostico,'catalogoDiag':catalogoDiag,'menu':menu,'confirm_message':mensaje})
    else:
        consulta=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        diagnostico=Diagnosticos.objects.get(id_conultamedica=id_conultamedica)
        catalogoDiag=Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=3))
        perUser = Persona.objects.get(user=request.user)
        medico=Medico.objects.get(id_persona=perUser)
        return render(request,'app/ConsultaMedica/editarConsulta.html',{'consulta':consulta,'medico':medico,'diagnostico':diagnostico,'catalogoDiag':catalogoDiag,'menu':menu})

@login_required()
def listarCirugia(request):
  # menu
        perUser = Persona.objects.get(user=request.user)
        with connection.cursor() as cursor:
         cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
         row = cursor.fetchone()
        menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
       # menu
        try:
            ciru = Catalogo.objects.filter(id_tipo_catalogo=1)
            if request.method == 'POST':
                perUser = Persona.objects.get(user=request.user)
                medico = Medico.objects.get(id_persona=perUser)
                expediente = request.POST['epaciente']
                fecha=request.POST['fecha']
                tcirugia=request.POST['tcirugia']
                if tcirugia!='':
                   idCirugia=Catalogo.objects.get(codigo=tcirugia)
                else:
                    idCirugia=''
                ciru = Catalogo.objects.filter(id_tipo_catalogo=1)
                if expediente!='' and fecha!='' and tcirugia!='':
                    paciente = Paciente.objects.get(codigo_expediente=expediente)
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico,fecha_consulta=fecha).values_list('id_conultamedica'),id_catalogo=idCirugia.id_catalogo).order_by('-id_conultamedica')
                    return render(request, 'app/Cirugia/listarCirugia.html',{'ciru': ciru, 'cirugia': cirugia, 'menu': menu})
                if expediente!='' and fecha!='':
                    paciente = Paciente.objects.get(codigo_expediente=expediente)
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico,fecha_consulta=fecha).values_list('id_conultamedica')).order_by('-id_conultamedica')
                    return render(request, 'app/Cirugia/listarCirugia.html', {'ciru':ciru,'cirugia': cirugia, 'menu': menu})
                if fecha!='' and tcirugia!='':
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_medico=medico.id_medico,fecha_consulta=fecha).values_list('id_conultamedica'),id_catalogo=idCirugia.id_catalogo).order_by('-id_conultamedica')
                    return render(request, 'app/Cirugia/listarCirugia.html',{'ciru': ciru, 'cirugia': cirugia, 'menu': menu})
                if expediente!='' and tcirugia!='':
                    paciente = Paciente.objects.get(codigo_expediente=expediente)
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico).values_list('id_conultamedica'),id_catalogo=idCirugia.id_catalogo).order_by('-id_conultamedica')
                    return render(request, 'app/Cirugia/listarCirugia.html',{'ciru': ciru, 'cirugia': cirugia, 'menu': menu})


                if expediente!='':
                    paciente = Paciente.objects.get(codigo_expediente=expediente)
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico).values_list('id_conultamedica')).order_by('-id_conultamedica')
                    return render(request, 'app/Cirugia/listarCirugia.html', {'ciru':ciru,'cirugia': cirugia, 'menu': menu})
                if fecha!='':
                    cirugia = ProgramacionCirugia.objects.filter(id_conultamedica__in=ConsultaMedica.objects.filter(id_medico=medico.id_medico,fecha_consulta=fecha).values_list('id_conultamedica')).order_by('-id_conultamedica')
                    ciru = Catalogo.objects.filter(id_tipo_catalogo=1)
                    return render(request, 'app/Cirugia/listarCirugia.html', {'ciru':ciru,'cirugia': cirugia, 'menu': menu})
                if tcirugia!='':
                    cirugia=ProgramacionCirugia.objects.filter(id_catalogo__in=Catalogo.objects.filter(codigo=tcirugia).values_list('id_catalogo'))
                    return render(request, 'app/Cirugia/listarCirugia.html', {'ciru':ciru,'cirugia': cirugia, 'menu': menu})
                else:
                    paciente = Paciente.objects.get(codigo_expediente=0)
            else:
                context = {'ciru':ciru,'menu':menu}
                return render(request,'app/Cirugia/listarCirugia.html',context)
        except(KeyError, Paciente.DoesNotExist, ConsultaMedica.DoesNotExist):
            return render(request, 'app/Cirugia/listarCirugia.html', {
                'error_message': "No Existen Registros de Cirugias",'menu':menu,'ciru':ciru})
@login_required()
def editarCirugia(request, id_conultamedica):
    # menu
        perUser = Persona.objects.get(user=request.user)
        with connection.cursor() as cursor:
          cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
          row = cursor.fetchone()
        menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        # menu
        descripcion=request.POST['descripcion']
        fecha=request.POST['fecha']
        hora=request.POST['hora']
        procedimiento=request.POST['Pcirugia']
        ncirugia=Cirugias(id_cirugia2=id_conultamedica,id_catalogo=Catalogo.objects.get(codigo=request.POST['ncirugia']),id_paciente=Paciente.objects.get(id_paciente=request.POST['npa']),descripcion=descripcion,fecha=fecha,procedimiento=procedimiento)
        ncirugia.save()
        ncirugia1=Cirugias.objects.get(id_cirugia2=ncirugia.id_cirugia2)
        Pcirugia=ProgramacionCirugia(id_catalogo=Catalogo.objects.get(codigo=request.POST['ncirugia']),fecha_programada=fecha,hora=hora,id_conultamedica_id=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica))
        Pcirugia.save()
        id=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        Pcirugia1=ProgramacionCirugia.objects.get(id_conultamedica=id.id_conultamedica)
        mensaje='Actualizado Correctamente'
        return render(request,'app/Cirugia/editarCirugia.html',{'cirugia':Pcirugia1,'dcirugia':ncirugia1,'menu':menu,'confirm_message':mensaje})

@login_required()
def verCirugia(request,id_conultamedica):#manda la lista de Programacion_cirugia
    # menu
      perUser = Persona.objects.get(user=request.user)
      with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
      menu = Recurso.objects.filter(
      id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
      # menu
      try:
            ncirugia=  Cirugias.objects.get(id_cirugia2=id_conultamedica).DoesNotExist
            si='no'
      except(KeyError,Cirugias.DoesNotExist):
            si='si'

      if si=='si':
            Pcirugia=ProgramacionCirugia.objects.get(id_conultamedica=ConsultaMedica(id_conultamedica=id_conultamedica))
            return render(request, 'app/Cirugia/editarCirugia.html', {'cirugia': Pcirugia,'menu':menu})
      else:
            Pcirugia = ProgramacionCirugia.objects.get(id_conultamedica=id_conultamedica)
            ncirugia = Cirugias.objects.get(id_cirugia2=id_conultamedica)
            return render(request, 'app/Cirugia/editarCirugia.html', {'cirugia': Pcirugia, 'dcirugia': ncirugia,'menu':menu})

@login_required()
def detallarCirugia(request, id_conultamedica):  # manda la lista de Programacion_cirugia
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
        ncirugia = Cirugias.objects.get(id_cirugia2=id_conultamedica).DoesNotExist
        si = 'no'
    except(KeyError, Cirugias.DoesNotExist):
        si = 'si'

    if si == 'si':
        Pcirugia = ProgramacionCirugia.objects.get(id_conultamedica=ConsultaMedica(id_conultamedica=id_conultamedica))
        return render(request, 'app/Cirugia/verCirugia.html', {'cirugia': Pcirugia,'menu':menu})
    else:
        Pcirugia = ProgramacionCirugia.objects.get(id_conultamedica=id_conultamedica)
        ncirugia = Cirugias.objects.get(id_cirugia2=id_conultamedica)
        return render(request, 'app/Cirugia/verCirugia.html', {'cirugia': Pcirugia, 'dcirugia': ncirugia,'menu':menu})
@login_required()
def eliminarConsulta(request, id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    consulta=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
    consulta.delete()
    return redirect('app:listarConsulta')

@login_required()
def eliminarCirugia(request, id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    cirugia=ProgramacionCirugia.objects.get(id_conultamedica=id_conultamedica)
    cirugia.delete()
    return redirect('app:listarcirugia')

@login_required()
def ingresoHospitalarioCreate(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu

    if request.method=='POST':
        try:
          expediente=request.POST['npaciente']
          nombre=request.POST['nompaciente']
          apellido=request.POST['apepaciente']
          if expediente!='':
             paciente=Paciente.objects.get(codigo_expediente=expediente)
          elif nombre!='' and apellido!='':
             per=Persona.objects.get(nombre=nombre,apellido=apellido)
             paciente=Paciente.objects.get(id_persona=per.id_persona)
          else:
             paciente=Paciente.objects.get(id_paciente=0)
          try:
                IngresoHospitalario.objects.get(id_paciente=paciente.id_paciente, activo='s')
                return render(request, 'app/ingresoHospitalario/IngresoHospitalario.html', {
                'error_message': "Verifique si el Paciente Tiene Activo un Ingreso Hospitalario", 'menu': menu})
          except(KeyError, IngresoHospitalario.DoesNotExist):
                catalogoIngreso = Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=7))
                catalogoSala = Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=8))
                medico = Medico.objects.all()
                context = {'paciente': paciente, 'catalogoIngreso': catalogoIngreso, 'catalogoSala': catalogoSala,
                         'medico': medico, 'menu': menu}
                return render(request, 'app/ingresoHospitalario/IngresoHospitalario.html', context)

        except(KeyError, Paciente.DoesNotExist,Catalogo.DoesNotExist,IngresoHospitalario.DoesNotExist):
            return render(request, 'app/ingresoHospitalario/IngresoHospitalario.html', {
                'error_message': "Ocurrio un Error", 'menu': menu})
    else:
        context={'menu':menu}
        return render(request,'app/ingresoHospitalario/IngresoHospitalario.html',context)
@login_required()
def guardarIngresoHospitalario(request):
    paciente=request.POST['pacienteid']
    ingreso=request.POST['ingreso']
    salon=request.POST['salon']
    fecha=request.POST['fecha']
    numcuarto=request.POST['numcuarto']
    numcamilla=request.POST['numcamilla']
    if numcuarto=='':
        numcuarto=0
    if numcamilla=='':
        numcamilla=0
    medico=request.POST['medico']
    medicamento=request.POST['medicamento']
    alergias=request.POST['alergias']
    reanimacion=request.POST['reanimacion']
    ingresopaciente=IngresoHospitalario(id_paciente=Paciente.objects.get(id_paciente=paciente),
                                        id_catalogo=Catalogo.objects.get(id_catalogo=ingreso),
                                        cat_codigo=Catalogo.objects.get(id_catalogo=salon),
                                        fecha_ingreso=fecha,numcuarto=numcuarto,numcamilla=numcamilla,
                                        id_medico=Medico.objects.get(id_medico=medico),
                                        medicamentos=medicamento,alergias=alergias,reanimacion=reanimacion,activo='s')
    ingresopaciente.save()
    return redirect('app:ingresoHospitalario')
@login_required()
def listIngresoHospitalario(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        try:
           expediente=request.POST['hpaciente']
           fecha=request.POST['hfecha']
           alta=request.POST['alta']
           if expediente!='' and  fecha!='' and alta!='':
              ingreso=IngresoHospitalario.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                                        fecha_ingreso=fecha,activo=alta).order_by('-fecha_ingreso')
              context={'menu':menu,'ingreso':ingreso}
              return render(request,'app/ingresoHospitalario/GestionarIngresoHospitalario.html',context)
           elif expediente!='' and fecha!='':
              ingreso = IngresoHospitalario.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                                         fecha_ingreso=fecha,activo='s').order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           elif expediente!='' and alta!='':
              ingreso = IngresoHospitalario.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                                         activo=alta).order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           elif fecha!='' and alta!='':
              ingreso = IngresoHospitalario.objects.filter(fecha_ingreso=fecha,activo=alta).order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           elif expediente!='':
              ingreso = IngresoHospitalario.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),activo='s').order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           elif fecha!='':
              ingreso = IngresoHospitalario.objects.filter(fecha_ingreso=fecha).order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           elif alta!='':
              ingreso = IngresoHospitalario.objects.filter(activo=alta).order_by('-fecha_ingreso')
              context = {'menu': menu, 'ingreso': ingreso}
              return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', context)
           else:
               ingreso=IngresoHospitalario.objects.get(activo='no')
        except(KeyError, IngresoHospitalario.DoesNotExist,Paciente.DoesNotExist):
            return render(request, 'app/ingresoHospitalario/GestionarIngresoHospitalario.html', {
                'error_message': "No Existen Datos Solicitados", 'menu': menu})

    else:
        context={'menu':menu}
        return render(request,'app/ingresoHospitalario/GestionarIngresoHospitalario.html',context)

@login_required()
def detalleIngresoHopsitalario(request,id_ingresohospitalario2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    ingreso=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
    context = {'menu': menu,'ingreso':ingreso}
    return render(request, 'app/ingresoHospitalario/DetalleIngresoHospitalario.html', context)
@login_required()
def editIngresoHospitalario(request,id_ingresohospitalario2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        paciente=request.POST['hpaciente']
        ingreso = request.POST['ingreso']
        salon = request.POST['salon']
        fecha = request.POST['fecha']
        numcuarto = request.POST['numcuarto']
        numcamilla = request.POST['numcamilla']
        if numcuarto=='':
            numcuarto=0
        if numcamilla=='':
            numcamilla=0
        medico = request.POST['medico']
        medicamento = request.POST['medicamento']
        alergias = request.POST['alergias']
        reanimacion = request.POST['reanimacion']
        ingresopaciente = IngresoHospitalario(id_ingresohospitalario2=id_ingresohospitalario2,
                                              id_paciente=Paciente.objects.get(id_paciente=paciente),
                                              id_catalogo=Catalogo.objects.get(id_catalogo=ingreso),
                                              cat_codigo=Catalogo.objects.get(id_catalogo=salon),
                                              fecha_ingreso=fecha, numcuarto=numcuarto, numcamilla=numcamilla,
                                              id_medico=Medico.objects.get(id_medico=medico),
                                              medicamentos=medicamento, alergias=alergias, reanimacion=reanimacion,
                                              activo='s')
        ingresopaciente.save()
        ingreso = IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        medico = Medico.objects.all()
        catalogoIngreso = Catalogo.objects.filter(id_tipo_catalogo=7)
        catalogoSala = Catalogo.objects.filter(id_tipo_catalogo=8)
        mensaje='Actualizado Correctamente'
        context = {'menu': menu, 'ingreso': ingreso, 'medico': medico, 'catalogoIngreso': catalogoIngreso,
                   'catalogoSala': catalogoSala,'confirm_message':mensaje}
        return render(request, 'app/ingresoHospitalario/EditarIngresoHospitalario.html', context)



    else:
        ingreso=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        medico=Medico.objects.all()
        catalogoIngreso=Catalogo.objects.filter(id_tipo_catalogo=7)
        catalogoSala=Catalogo.objects.filter(id_tipo_catalogo=8)
        context={'menu':menu,'ingreso':ingreso,'medico':medico,'catalogoIngreso':catalogoIngreso,'catalogoSala':catalogoSala}
        return render(request,'app/ingresoHospitalario/EditarIngresoHospitalario.html',context)

@login_required()
def altaMedica(request,id_ingresohospitalario2):
    if request.method=='POST':
        paciente=request.POST['hpaciente']
        alta=request.POST['tipoAlta']
        condicion=request.POST['condicion']
        cuidados=request.POST['cuidados']
        fecha=request.POST['fecha']
        altaMedica=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        altaMedica.tipo_alta=alta
        altaMedica.condicion_salida=condicion
        altaMedica.cuidados=cuidados
        altaMedica.fecha_salida=fecha
        altaMedica.activo='n'
        altaMedica.save()
        return redirect('app:listarIngresoHospitalario')
    else:
        # menu
        perUser = Persona.objects.get(user=request.user)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
            row = cursor.fetchone()
        menu = Recurso.objects.filter(
            id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
        # menu
        ingreso=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        context={'menu':menu,'ingreso':ingreso}
        return render(request,'app/ingresoHospitalario/AltaMedica.html',context)

@login_required()
def BuscarIngresoServicioHospitalario(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu

    if request.method=='POST':
        try:
           expediente=request.POST['hpaciente']
           ingreso=IngresoHospitalario.objects.get(id_paciente=Paciente.objects.get(codigo_expediente=expediente),activo='s')
           context={'menu':menu,'ingreso':ingreso}
           return render(request,'app/ingresoHospitalario/AsignacionServicioHospitalario.html',context)
        except(KeyError,IngresoHospitalario.DoesNotExist,Paciente.DoesNotExist):
            return render(request, 'app/ingresoHospitalario/AsignacionServicioHospitalario.html', {
                'error_message': "El Paciente Solicitado no Posee un Ingreso Hospitalario Activo", 'menu': menu})
    else:
        context={'menu':menu}
        return render(request,'app/ingresoHospitalario/AsignacionServicioHospitalario.html',context)


@login_required()
def CreateServicioHospitalario(request, id_ingresohospitalario2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method == 'POST':
        responsable=request.POST['responsable']
        catalogo=request.POST['cata']
        fecha=request.POST['fecha']
        duracion=request.POST['duracion']
        dosis=request.POST['dosis']
        servicio_ospitalario=ServicioHospitalario(id_medico=Medico.objects.get(id_medico=responsable),
                                                  id_ingresohospitalario2=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2),
                                                  id_catalogo=Catalogo.objects.get(id_catalogo=catalogo),
                                                  fecha_inicio=fecha,duracion=duracion,dosis=dosis)
        servicio_ospitalario.save()
        mensaje='Guardado con Éxito'
        fecha = timezone.now().date().isoformat()
        ingreso = IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        catalogo = Catalogo.objects.filter(id_tipo_catalogo__in=TipoCatalogo.objects.filter(id_tipo_catalogo__in={5, 10}).values_list('id_tipo_catalogo'))
        responsable = Medico.objects.filter(id_catalogo=Catalogo.objects.get(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=9)))
        context = {'fecha': fecha, 'catalogo': catalogo, 'responsable': responsable, 'menu': menu, 'ingreso': ingreso,'confirm_message':mensaje}
        return render(request, 'app/ingresoHospitalario/IngresarServicioHospitalario.html', context)
    else:
        fecha = timezone.now().date().isoformat()
        ingreso=IngresoHospitalario.objects.get(id_ingresohospitalario2=id_ingresohospitalario2)
        catalogo=Catalogo.objects.filter(id_tipo_catalogo__in=TipoCatalogo.objects.filter(id_tipo_catalogo__in={5,10}).values_list('id_tipo_catalogo'))
        responsable=Medico.objects.filter(id_catalogo=Catalogo.objects.get(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=9)))
        context={'fecha':fecha,'catalogo':catalogo,'responsable':responsable,'menu':menu,'ingreso':ingreso}
        return  render(request,'app/ingresoHospitalario/IngresarServicioHospitalario.html',context)

@login_required()
def ListarServiciosHospitalarios(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        try:
           paciente=request.POST['hpaciente']
           servicio=ServicioHospitalario.objects.filter(id_ingresohospitalario2=IngresoHospitalario.objects.get(id_paciente=Paciente.objects.get(codigo_expediente=paciente),activo='s'))
           context={'menu':menu,'servicio':servicio}
           return render(request,'app/ingresoHospitalario/ListarServicioHospitalario.html',context)
        except(KeyError,Paciente.DoesNotExist,IngresoHospitalario.DoesNotExist,ServicioHospitalario.DoesNotExist):
            return render(request, 'app/ingresoHospitalario/ListarServicioHospitalario.html', {
                'error_message': "No se Encontraron Datos Del Paciente", 'menu': menu})
    else:
        context={'menu':menu}
        return render(request,'app/ingresoHospitalario/ListarServicioHospitalario.html',context)

@login_required()
def EditarServicioHospitalario(request,id_servicio_hospitalario):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        tipoServicio=request.POST['servicioh']
        responsable=request.POST['responsable']
        fecha=request.POST['fecha']
        dosis=request.POST['dosis']
        duracion=request.POST['duracion']
        servicioH=ServicioHospitalario.objects.get(id_servicio_hospitalario=id_servicio_hospitalario)
        servicioH.id_catalogo=Catalogo.objects.get(id_catalogo=tipoServicio)
        servicioH.id_medico=Medico.objects.get(id_medico=responsable)
        servicioH.fecha_inicio=fecha
        servicioH.dosis=dosis
        servicioH.duracion=duracion
        servicioH.save()
        mensaje='actualizado correctamente'
        catalogo = Catalogo.objects.filter(
            id_tipo_catalogo__in=TipoCatalogo.objects.filter(id_tipo_catalogo__in={5, 10}).values_list(
                'id_tipo_catalogo'))
        responsable = Medico.objects.filter(
            id_catalogo=Catalogo.objects.get(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=9)))
        servicio = ServicioHospitalario.objects.get(id_servicio_hospitalario=id_servicio_hospitalario)
        context = {'menu': menu, 'catalogo': catalogo, 'responsable': responsable, 'servicio': servicio,'confirm_message':mensaje}
        return render(request, 'app/ingresoHospitalario/EditarServicioHospitalario.html', context)
    else:
        catalogo = Catalogo.objects.filter(
            id_tipo_catalogo__in=TipoCatalogo.objects.filter(id_tipo_catalogo__in={5, 10}).values_list(
                'id_tipo_catalogo'))
        responsable = Medico.objects.filter(
            id_catalogo=Catalogo.objects.get(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=9)))
        servicio = ServicioHospitalario.objects.get(id_servicio_hospitalario=id_servicio_hospitalario)
        context={'menu':menu,'catalogo':catalogo,'responsable':responsable,'servicio':servicio}
        return render(request,'app/ingresoHospitalario/EditarServicioHospitalario.html',context)

@login_required()
def EliminarServicioHospitalario(request,id_servicio_hospitalario):
    servicio=ServicioHospitalario.objects.get(id_servicio_hospitalario=id_servicio_hospitalario)
    servicio.delete()
    return redirect('app:listarServicioHospitalario')

@login_required()
def upload_file(request,id_examen):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method == 'POST':
        form = AdjuntoForm(request.POST, request.FILES)
        if form.is_valid():

           examen=request.POST['idexa']
           imagen=request.FILES.get('imagen','')
           video=request.FILES.get('video','')
           audio=request.FILES.get('audio','')

           adjunto=Adjuntos(id_examen=Examen.objects.get(id_examen=examen),imagen=imagen,video=video,audio=audio)
           adjunto.save()
           return redirect("app:buscarExamen")
    else:
            form = AdjuntoForm()
            examen=Examen.objects.get(id_examen=id_examen)
            return render(request, 'app/RegistroDeExamenes/IngresarResultadoExamen.html', {'form': form,'menu':menu,'examen':examen})
@login_required()
def IngresarDatosDeResultadoExamen(request,id_examen):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    perUser = Persona.objects.get(user=request.user)
    examen = Examen.objects.get(id_examen=id_examen)
    if request.method == 'POST':
        encargado = perUser.nombre + ' ' + perUser.apellido
        descripcion = request.POST['descripcion']
        resultado1 = request.POST['resultado']
        observacion = request.POST['observacion']
        try:
            resultado = Resultados.objects.get(id_examen=Examen.objects.get(id_examen=id_examen))
            resultado.descripcion = descripcion
            resultado.resultado = resultado1
            resultado.observacion = observacion
            resultado.save()
            mensaje = 'Actualizado Correctamente, Puede Actualizar Nuevamente'
            context = {'menu': menu, 'resultado': resultado, 'persona': perUser, 'examen': examen,
                           'confirm_message': mensaje}
            return render(request, 'app/RegistroDeExamenes/IngresarDatosDeResultadoExamen.html', context)
        except(KeyError, Resultados.DoesNotExist, Examen.DoesNotExist):
            resultado = Resultados(id_examen=Examen.objects.get(id_examen=id_examen), encargado=encargado,
                                       descripcion=descripcion,
                                       resultado=resultado1, observacion=observacion)
            mensaje = 'Guardado Correctamente, Puede Editar'
            resultado.save()
            context = {'menu': menu, 'resultado': resultado, 'persona': perUser, 'examen': examen,
                           'confirm_message': mensaje}
            return render(request, 'app/RegistroDeExamenes/IngresarDatosDeResultadoExamen.html', context)
    else:
        try:
            resultado=Resultados.objects.get(id_examen=Examen.objects.get(id_examen=id_examen))
            context = {'menu': menu, 'persona': perUser, 'examen': examen,'resultado':resultado}
            return render(request, 'app/RegistroDeExamenes/IngresarDatosDeResultadoExamen.html', context)
        except(KeyError,Resultados.DoesNotExist):
            context = {'menu': menu, 'persona': perUser, 'examen': examen}
            return render(request, 'app/RegistroDeExamenes/IngresarDatosDeResultadoExamen.html', context)

@login_required()
def ListarDatosExamenesHistoricos(request,id_examen):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    examen = Examen.objects.get(id_examen=id_examen)
    try:
        resultado=Resultados.objects.get(id_examen=Examen.objects.get(id_examen=id_examen))
        context={'menu':menu,'resultado':resultado}
        return render(request,'app/RegistroDeExamenes/HistorialDatosExamenes.html',context)
    except(KeyError,Resultados.DoesNotExist):
        mensaje='Nose registraron Datos'
        context={'menu':menu,'examen':examen,'message_error':mensaje}
        return render(request,'app/RegistroDeExamenes/HistorialDatosExamenes.html',context)


@login_required()
def ListarExamenMedico(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        expediente=request.POST['expaciente']
        fecha=request.POST['fecha']
        try:
           if expediente!='':
              examen=Examen.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),exa_realizado='n')
              context = {'menu': menu, 'examen': examen}
              return render(request, 'app/RegistroDeExamenes/ExamenesPendientesDeRealizar.html', context)
           if fecha!='':
              examen=Examen.objects.filter(fecha=fecha,exa_realizado='n')
           else:
              examen=''
           context={'menu':menu,'examen':examen}
           return render(request, 'app/RegistroDeExamenes/ExamenesPendientesDeRealizar.html', context)
        except(KeyError,Paciente.DoesNotExist,Examen.DoesNotExist):
            return render(request, 'app/RegistroDeExamenes/ExamenesPendientesDeRealizar.html', {
                'error_message': "No Existen Datos Asociados al Paciente", 'menu': menu})


    else:
        examen=Examen.objects.filter(exa_realizado='n')
        context={'menu':menu,'examen':examen}
        return  render(request,'app/RegistroDeExamenes/ExamenesPendientesDeRealizar.html',context)
@login_required()
def post_edit(request, id_adjunto):
    # menu
     perUser = Persona.objects.get(user=request.user)
     with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
     menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
     # menu
     if request.method == "POST":
        form = AdjuntoForm(request.POST, request.FILES)
        if form.is_valid():
            imagen = request.FILES.get('imagen','')
            video = request.FILES.get('video','')
            audio = request.FILES.get('audio','')
            adjunto = Adjuntos.objects.get(id_adjunto=id_adjunto)
            if imagen!='':
                adjunto.imagen = imagen
            if video!='':
                adjunto.video = video
            if audio!='':
                adjunto.audio = audio
            adjunto.save()
            return redirect("app:buscarExamen")
     else:
        context={'menu':menu,'message_error':'error'}
        return redirect('app:buscarExamen')


@login_required()
def ExamenRealizado(request,id_examen):
    examen=Examen.objects.get(id_examen=id_examen)
    fecha=timezone.now().date().isoformat()
    examen.exa_realizado='s'
    examen.fecha_realizado=fecha
    examen.save()
    return redirect('app:buscarExamen')
@login_required()
def ExamenesHistoricos(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu

    try:
       if request.method=='POST':
          expediente = request.POST['expaciente']
          fecha = request.POST['fecha']
          if expediente!='' and fecha!='':
             examen=Examen.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                         fecha_realizado=fecha,exa_realizado='s').order_by('fecha_realizado','-id_examen')
             context={'menu':menu,'examen':examen}
             return render(request,'app/RegistroDeExamenes/HistorialExamenes.html',context)
          if expediente!='':
             examen=Examen.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),exa_realizado='s').\
                 order_by('-id_examen')
             context={'menu':menu,'examen':examen}
             return render(request, 'app/RegistroDeExamenes/HistorialExamenes.html', context)
          if fecha!='':
             examen=Examen.objects.filter(fecha_realizado=fecha,exa_realizado='s').order_by('-id_examen')
             context = {'menu': menu, 'examen': examen}
             return render(request, 'app/RegistroDeExamenes/HistorialExamenes.html', context)
       else:
          examen=Examen.objects.filter(exa_realizado='s').order_by('-id_examen')
          context = {'menu': menu, 'examen': examen}
          return render(request, 'app/RegistroDeExamenes/HistorialExamenes.html', context)
    except(KeyError, Paciente.DoesNotExist, Examen.DoesNotExist):
        return render(request, 'app/RegistroDeExamenes/HistorialExamenes.html', {
            'error_message': "No Existen Datos Asociados al Paciente", 'menu': menu})

@login_required()
def VistaDeArchivosHistoricos(request,id_examen):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
        examen = Examen.objects.get(id_examen=id_examen)
        adjunto = Adjuntos.objects.filter(id_examen=examen.id_examen)
        context={'menu':menu,'examen':adjunto}
        return render(request,'app/RegistroDeExamenes/VistaDeArchivos.html',context)
    except(KeyError, Examen.DoesNotExist, Adjuntos.DoesNotExist):
        mensaje='No Existen Archivos'
        context={'menu':menu,'message_error':mensaje}
        return render(request,'app/RegistroDeExamenes/VistaDeArchivos.html',context)

@login_required()
def AsignarExamenMedico(request):
   # menu
   perUser = Persona.objects.get(user=request.user)
   with connection.cursor() as cursor:
    cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
    row = cursor.fetchone()
   menu = Recurso.objects.filter(
    id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
   # menu
   try:
      if request.method=='POST':
        expediente=request.POST['epaciente']
        perUser = Persona.objects.get(user=request.user)
        medico = Medico.objects.get(id_persona=perUser)
        consulta = ConsultaMedica.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                           id_medico=medico).order_by('-fecha_consulta','-id_conultamedica')[:1]
        context={'menu':menu,'consulta':consulta}
        return render(request,'app/ExamenesDoctor/CrearExamen.html',context)
      else:
        context={'menu':menu}
        return render(request,'app/ExamenesDoctor/CrearExamen.html',context)
   except(KeyError,Medico.DoesNotExist,Paciente.DoesNotExist,ConsultaMedica.DoesNotExist):
       return render(request, 'app/ExamenesDoctor/CrearExamen.html', {
           'error_message': "El Paciente no Existe o No Hay Registros de Él", 'menu': menu})

@login_required()
def AsignarExamenAPacietne(request,id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':

        id_paciente=request.POST['epaciente']
        catalogo=request.POST['catalogo']
        try:
            exa=Examen.objects.get(id_paciente=Paciente.objects.get(id_paciente=id_paciente),
                                   id_catalogo=Catalogo.objects.get(id_catalogo=catalogo),exa_revisado='n',
                                   exa_realizado='n')
            catalogo = Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=10))
            consulta = ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
            context = {'menu': menu, 'catalogo': catalogo, 'consulta': consulta,'message_error':'El Paciente Ya tiene Asiganado un Examen Activo'}
            return render(request, 'app/ExamenesDoctor/AsignarExamenAPaciente.html', context)
        except(KeyError,Examen.DoesNotExist):
            hola=''
        fecha=timezone.now().date().isoformat()
        examen=Examen(id_conultamedica=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica),
                      id_paciente=Paciente.objects.get(id_paciente=id_paciente),
                      fecha=fecha,id_catalogo=Catalogo.objects.get(id_catalogo=catalogo))
        examen.save()
        mensaje='Guardado Correctamente'
        catalogo = Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=10))
        consulta = ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        context = {'menu': menu, 'catalogo': catalogo, 'consulta': consulta,'confirm_message':mensaje}
        return render(request, 'app/ExamenesDoctor/AsignarExamenAPaciente.html', context)
    else:
        catalogo=Catalogo.objects.filter(id_tipo_catalogo=TipoCatalogo.objects.get(id_tipo_catalogo=10))
        consulta=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
        context={'menu':menu,'catalogo':catalogo,'consulta':consulta}
        return render(request,'app/ExamenesDoctor/AsignarExamenAPaciente.html',context)

@login_required()
def ListarExamenesActivos(request,id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
       examen=Examen.objects.filter(id_conultamedica=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica),
                                 exa_realizado='n')
       context={'menu':menu,'examen':examen}
       return render(request,'app/ExamenesDoctor/ListarExamenesActivos.html',context)
    except(Examen.DoesNotExist):
        context = {'menu': menu}
        return render(request, 'app/ExamenesDoctor/ListarExamenesActivos.html', context)
@login_required()
def EliminarExamenActivo(request,id_examen):
    examen=Examen.objects.get(id_examen=id_examen)
    examen.delete()
    return redirect('app:AsignarExamenMedico')



@login_required()
def ResultadosExamenesView(request):
    data =dict()
    id_examen=request.GET['id_examen']
    resultado=Resultados.objects.get(id_resultado=id_examen)
    context={'resultado':resultado}
    data['html_form'] = render_to_string('app/ConsultaMedica/modales/viewResultado.html', context=context, request=request)
    return JsonResponse(data)
@login_required()
def ArchivosModalesView(request):
    data =dict()
    id_examen=request.GET['id_examen']
    adjunto = Adjuntos.objects.filter(id_examen=Examen.objects.get(id_examen=id_examen))
    examen=Examen.objects.get(id_examen=id_examen)
    context={'adjunto':adjunto,'examen':examen}
    data['html_form'] = render_to_string('app/ConsultaMedica/modales/viewArchivos.html', context=context, request=request)
    return JsonResponse(data)

@login_required()
def BuscarPacienteSignoVital(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    fecha = timezone.now().date().isoformat()

    if request.method=='POST':
        expediente = request.POST['epaciente']
        try:
            signoVital = SignosVitales.objects.get(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                                   med='n', fecha=fecha)
            mensaje = 'El paciente Tiene un Registro de Signo Vital Activo'
            paciente = Paciente.objects.get(codigo_expediente=expediente)
            context = {'menu': menu, 'mensaje': mensaje, 'signo': signoVital,'paciente':paciente}
            return render(request, 'app/signosVitales/BuscarPaciente.html', context)
        except(KeyError, Paciente.DoesNotExist, SignosVitales.DoesNotExist):
            try:
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              context={'menu':menu,'paciente':paciente,'fecha':fecha}
              return render(request,'app/signosVitales/BuscarPaciente.html',context)
            except(KeyError,Paciente.DoesNotExist):
                return render(request, 'app/ExamenesDoctor/CrearExamen.html', {
                    'error_message': "El Paciente no Existe", 'menu': menu})
    else:
       context={'menu':menu}
       return render(request,'app/signosVitales/BuscarPaciente.html',context)

@login_required()
def CrearSignoVital(request,id_paciente):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        peso= request.POST['peso']
        estatura=request.POST['estatura']
        presion=request.POST['presion']
        ritmo=request.POST['ritmo']
        frecuencia=request.POST['frecuencia']
        fecha=request.POST['fecha']
        signo=SignosVitales(id_paciente=Paciente.objects.get(id_paciente=id_paciente),peso=peso,estatura=estatura,
                            presion_arterial=presion,ritmo_cardiaco=ritmo,frecuencia_respiratoria=frecuencia,
                            fecha=fecha)
        signo.save()
        return redirect('app:BuscarPacienteSignoVital')

    else:
        fecha=timezone.now().date().isoformat()
        paciente=Paciente.objects.get(id_paciente=id_paciente)
        context={'menu':menu,'fecha':fecha,'paciente':paciente}
        return render(request,'app/signosVitales/CrearSignoVital.html',context)

@login_required()
def EditarSignoVital(request, id_paciente):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    fecha=timezone.now().date().isoformat()
    if request.method=='POST':
        signo=SignosVitales.objects.get(id_paciente=Paciente.objects.get(id_paciente=id_paciente),
                                                   med='n', fecha=fecha)
        signo.peso=request.POST['peso']
        signo.estatura=request.POST['estatura']
        signo.presion_arterial=request.POST['presion']
        signo.ritmo_cardiaco=request.POST['ritmo']
        signo.frecuencia_respiratoria=request.POST['frecuencia']
        signo.save()
        mensaje = 'Datos Guardados'
        context = {'menu': menu, 'signo': signo,'confirm_message':mensaje}

        return render(request, 'app/signosVitales/EditarSignoVital.html', context)
    else:
        signo=SignosVitales.objects.get(id_paciente=Paciente.objects.get(id_paciente=id_paciente),
                                                   med='n', fecha=fecha)
        print(signo)
        context={'menu':menu,'signo':signo}
        return render(request,'app/signosVitales/EditarSignoVital.html',context)

@login_required()
def EliminarSignoVital(request, id_paciente):
    fecha = timezone.now().date().isoformat()
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    signo = SignosVitales.objects.get(id_paciente=Paciente.objects.get(id_paciente=id_paciente),
                                      med='n', fecha=fecha)
    signo.delete()
    return redirect('app:BuscarPacienteSignoVital')
@login_required()
def BuscarSignoVitalHistorico(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method=='POST':
        expediente=request.POST['epaciente']
        try:
           signo=SignosVitales.objects.filter(id_paciente=Paciente.objects.get(codigo_expediente=expediente),
                                           med='s')
           context={'menu':menu,'signo':signo}
           return render(request,'app/signosVitales/HistorialSignosVitales.html',context)
        except(KeyError,Paciente.DoesNotExist):
                return render(request, 'app/signosVitales/HistorialSignosVitales.html', {
                    'error_message': "El Paciente no Existe", 'menu': menu})
    else:
        context = {'menu': menu}
        return render(request, 'app/signosVitales/HistorialSignosVitales.html', context)

@login_required()
def VerHistorialSignoVital(request,id_signovital2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    signo=SignosVitales.objects.get(id_signovital2=id_signovital2)
    context={'menu':menu,'signo':signo}
    return render(request,'app/signosVitales/VerHistorialSignosVitales.html',context)




#aris.....................

@login_required()
def seguimientoCirugia(request,id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
        tipo_cir = Cirugias.objects.get(id_cirugia2=id_conultamedica)
    except (KeyError,Cirugias.DoesNotExist):
        tipo_cir = ''


    catalogo = Catalogo.objects.filter(id_tipo_catalogo=6)
    return render(request,'app/SeguimientoCirugia/seguimientoCirugia.html',{'tipo_cirugia':tipo_cir,'catalogoCirugia':catalogo,'menu':menu})

@login_required()
def guardarSegCirugia(request):
    id_tipo_cirugia=request.POST['id_tipo_cirugia']
    descripcion=request.POST['descripcion']
    fechaserv = request.POST['fechaserv']
    tipoSeg = request.POST['tipoSeg']
    seguimiento = SeguimientoCirugia(fecha_servicio=fechaserv,descripcion=descripcion,id_catalogo=Catalogo.objects.get(id_catalogo=tipoSeg),id_cirugia2=Cirugias.objects.get(id_cirugia2=id_tipo_cirugia))
    seguimiento.save()
    return redirect('app:listarcirugia')

@login_required()
def listarSeguimientos(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:

        if request.method == 'POST':
            perUser = Persona.objects.get(user=request.user)
            medico = Medico.objects.get(id_persona=perUser)
            expediente = request.POST['epaciente']

            if expediente != '':
                paciente = Paciente.objects.get(codigo_expediente=expediente)
                cirugia = Cirugias.objects.filter(
                    id_cirugia2__in=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,
                                                                       id_medico=medico.id_medico))
                return render(request, 'app/SeguimientoCirugia/seguimientoLista.html',
                              {'cirugia': cirugia, 'menu': menu})

        else:
            context = {'menu': menu}
            return render(request, 'app/SeguimientoCirugia/seguimientoLista.html', context)
    except(KeyError, Paciente.DoesNotExist, ConsultaMedica.DoesNotExist):
        return render(request, 'app/SeguimientoCirugia/seguimientoLista.html', {
            'error_message': "No Existen Registros de Seguimientos de Cirugias", 'menu': menu})

@login_required()
def seguimiento(request,id_cirugia2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    segCir=SeguimientoCirugia.objects.filter(id_cirugia2=Cirugias.objects.get(id_cirugia2=id_cirugia2))
    return render(request,'app/SeguimientoCirugia/listadoSeg.html',{'menu':menu,'segCirugias':segCir})


@login_required()
def detalleSeguimiento(request,id_postcir):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    segCirugia=SeguimientoCirugia.objects.get(id_postoperacion2=SeguimientoCirugia.objects.get(id_postoperacion2=id_postcir))
    return render(request, 'app/SeguimientoCirugia/editarSeguimiento.html', {'menu': menu, 'editarSeg': segCirugia})

@login_required()
def editarSeguimiento(request,id_postoperacion2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method == 'POST':
        descripcion = request.POST['descripcion']
        fecha_serv = request.POST['fecha_servicio']
        periodo = request.POST['periodo']
        seguimiento = SeguimientoCirugia.objects.get(id_postoperacion2=id_postoperacion2)
        seguimiento.descripcion=descripcion
        seguimiento.fecha_servicio=fecha_serv
        seguimiento.id_catalogo=Catalogo.objects.get(id_catalogo=periodo)
        seguimiento.save()
        id_seguim = SeguimientoCirugia.objects.get(id_postoperacion2=id_postoperacion2)
        catalogo = Catalogo.objects.filter(id_tipo_catalogo=6)
        confirmacion = 'Actualizado Correctamente'
        return render(request, 'app/SeguimientoCirugia/editarSeguimiento.html',{'catalogo':catalogo,'id_segui':id_seguim ,'menu': menu, 'confirm_message': confirmacion})
    else:
        id_seguim = SeguimientoCirugia.objects.get(id_postoperacion2=id_postoperacion2)
        catalogo = Catalogo.objects.filter(id_tipo_catalogo=6)
        return render(request, 'app/SeguimientoCirugia/editarSeguimiento.html',{'catalogo': catalogo, 'id_segui': id_seguim, 'menu': menu})

#----------------------
@login_required()
def tratamientoConsulta(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
       if request.method=='POST':

          perUser = Persona.objects.get(user=request.user)
          medico=Medico.objects.get(id_persona=perUser)
          expediente=request.POST['epaciente']
          fecha = request.POST['fecha']
          diagnostico=request.POST['diagnostico']
          if diagnostico!='':
             catalogo=Catalogo.objects.get(codigo=diagnostico)
          else:
              catalogo=''
          if expediente != '' and diagnostico != '' and fecha!='':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta1 = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico,fecha_consulta=fecha)
              consulta=consulta1.filter(id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica')).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {'consulta': consulta, 'menu': menu})

          if expediente != '' and fecha != '':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico,fecha_consulta=fecha).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html',{'consulta': consulta,  'menu': menu})

          if expediente != '' and diagnostico !='':
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta1 = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico)
              consulta = consulta1.filter(id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo)).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {'consulta': consulta, 'menu': menu})
          if diagnostico != '' and fecha!='':
              consulta = ConsultaMedica.objects.filter(id_medico=medico.id_medico,id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica'),fecha_consulta=fecha).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {'consulta': consulta, 'menu': menu})

          if expediente!='':
              paciente=Paciente.objects.get(codigo_expediente=expediente)
              consulta=ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente,id_medico=medico.id_medico).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html',{'consulta':consulta,'menu':menu})
          if diagnostico != '':
              consulta = ConsultaMedica.objects.filter(id_medico=medico.id_medico,id_conultamedica__in=Diagnosticos.objects.filter(id_catalogo=catalogo.id_catalogo).values_list('id_conultamedica')).order_by('-fecha_consulta')
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {'consulta': consulta, 'menu': menu})
          elif fecha!='':
             consulta= ConsultaMedica.objects.filter(fecha_consulta=fecha,id_medico=medico.id_medico).order_by('-fecha_consulta')
             return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {'consulta': consulta, 'menu': menu})
          else:
              paciente = Paciente.objects.get(codigo_expediente=expediente)
              consulta = ConsultaMedica.objects.filter(id_paciente=paciente.id_paciente, id_medico=medico.id_medico)
              return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html',{'consulta': consulta, 'menu': menu})



       else:
          template = loader.get_template('app/Tratamiento/buscarConsultaTratamiento.html')
          context = {'menu':menu}
          return HttpResponse(template.render(context, request))
    except(KeyError, Paciente.DoesNotExist, ConsultaMedica.DoesNotExist):

     return render(request, 'app/Tratamiento/buscarConsultaTratamiento.html', {
        'error_message': "No Existen Registros del Paciente",'menu':menu})

@login_required()
def guardarAsignacionTratamiento(request):
    id_conultamedica=request.POST['id_conultamedica']
    descripcion=request.POST['descripcion']
    dosisTra=request.POST['dosisT']
    fecha_servicio = request.POST['fecha_servicio']
    tipoTratamiento = request.POST['tipoTratamiento']
    tratamiento = Tratamientos(fecha_servicio=fecha_servicio,descripcion=descripcion,dosis=dosisTra,id_catalogo=Catalogo.objects.get(id_catalogo=tipoTratamiento),id_conultamedica=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica))
    tratamiento.save()
    return redirect('app:listarConsultaTratamiento')
@login_required()
def tratamientoAsignacion(request,id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    try:
        consulta_id = ConsultaMedica.objects.get(id_conultamedica=id_conultamedica)
    except (KeyError,ConsultaMedica.DoesNotExist):
        consulta_id = ''
    catalogo = Catalogo.objects.filter(id_tipo_catalogo=5)
    return render(request,'app/Tratamiento/asignarTratamiento.html',{'consulta_id':consulta_id,'catalogoTratamiento':catalogo,'menu':menu})


@login_required()
def tratamientosAsignados(request,id_conultamedica):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    tratAsignados=Tratamientos.objects.filter(id_conultamedica=ConsultaMedica.objects.get(id_conultamedica=id_conultamedica))
    return render(request,'app/Tratamiento/listaTratamientos.html',{'menu':menu,'tratAsignados':tratAsignados})

@login_required()
def detalleTratamiento(request,id_tratamiento2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    trata=Tratamientos.objects.get(id_tratamiento2=Tratamientos.objects.get(id_tratamiento2=id_tratamiento2))
    return render(request, 'app/Tratamiento/editarTratamiento.html', {'menu': menu, 'editarTratamiento': trata})

@login_required()
def editarTratamientos(request,id_tratamiento2):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    if request.method == 'POST':
        descripcion = request.POST['descripcion']
        dosisTra = request.POST['dosis']
        fecha_servicio = request.POST['fecha_servicio']
        tipoTratamiento = request.POST['tipoTratamiento']
        tratamiento = Tratamientos.objects.get(id_tratamiento2=id_tratamiento2)
        tratamiento.descripcion=descripcion
        tratamiento.fecha_servicio=fecha_servicio
        tratamiento.dosis=dosisTra
        tratamiento.id_catalogo=Catalogo.objects.get(id_catalogo=tipoTratamiento)
        tratamiento.save()
        id_trata = Tratamientos.objects.get(id_tratamiento2=id_tratamiento2)
        catalogo = Catalogo.objects.filter(id_tipo_catalogo=5)
        confirmacion = 'Actualizado Correctamente'
        return render(request,'app/Tratamiento/editarTratamiento.html',{'catalogo':catalogo,'id_trata':id_trata ,'menu': menu, 'confirm_message': confirmacion})
    else:
        id_trata = Tratamientos.objects.get(id_tratamiento2=id_tratamiento2)
        catalogo = Catalogo.objects.filter(id_tipo_catalogo=5)
        return render(request,'app/Tratamiento/editarTratamiento.html',{'catalogo': catalogo,'id_trata':id_trata , 'menu': menu})

@login_required()
def eliminarTratamiento(request, id_tratamiento2):
    tratamiento=Tratamientos.objects.get(id_tratamiento2=id_tratamiento2)
    tratamiento.delete()
    return redirect('app:listarConsultaTratamiento')
