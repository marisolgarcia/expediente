from django import forms
from django.forms import ClearableFileInput

from .models import Cita, Catalogo, Medico, ServicioHospitalario, Adjuntos


class CitaForm(forms.ModelForm):
    codigo = forms.ModelChoiceField(queryset=Catalogo.objects.filter(id_tipo_catalogo=2).all(), label='Tipo Cita')
    id_medico = forms.ModelChoiceField(queryset=Medico.objects.all(), label='Médico')
    class Meta:
        model = Cita
        exclude = ['id_cita','fecha_registro']

class CustomClearableFileInput(ClearableFileInput):
    template_with_clear = ' <label for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</label> %(clear)s'

class AdjuntoForm(forms.ModelForm):
    class Meta:
        model =Adjuntos

        fields=[
            'id_examen',
            'imagen',
            'video',
            'audio',
        ]

        labels={
            'id_examen':'Nombre del Examen',
            'imagen': 'Subir Imagen',
            'video':'Subir Video',
            'audio':'Subir Audio',
        }

        widgets={
            'id_examen':forms.TextInput(attrs={'class':'form-control'}),
            'imagen': CustomClearableFileInput,
            'video': CustomClearableFileInput,
            'audio': CustomClearableFileInput,

        }