from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Paciente,GrupoRecurso, Persona, Familiares, Catalogo, Padecimientos, Recurso, Bitacora, ServicioHospitalario
import datetime
from django.views import generic
from django.contrib.auth.decorators import  login_required
from django.shortcuts import redirect
from django.db import connection
from django.contrib.auth.models import User


def indexPaciente(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    codigo = request.POST['codigo']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']

    if codigo ==''and nombre==''and apellido=='':
        context = {'error_message':'No ha ingresado datos para la busqueda', 'menu':menu}
        return render(request,'app/paciente/buscarPaciente.html',context)

    if codigo!='':
        paciente = Paciente.objects.filter(codigo_expediente=codigo)
        if not paciente.exists():
            context = {'error_message': 'Paciente no encontrado', 'menu': menu}
            return render(request, 'app/paciente/buscarPaciente.html', context)
        else:
            context = {'message': 'Informacion de paciente', 'menu': menu, 'pacientes': paciente}
            return render(request, 'app/paciente/pacienteList.html', context)
    else:
        if nombre!='' and apellido!='':
            try:
                per = Persona.objects.get(nombre=nombre, apellido=apellido)
                paciente = Paciente.objects.filter(id_persona=per.id_persona)
                context = {'message': 'Informacion de paciente', 'menu': menu, 'pacientes': paciente}
                return render(request, 'app/paciente/pacienteList.html', context)

            except(KeyError,Persona.DoesNotExist):
                context = {'error_message': 'Paciente no encontrado', 'menu': menu}
                return render(request, 'app/paciente/buscarPaciente.html', context)
        else:
            context = {'error_message':'Debe ingresar codigo expediente o nombre y apellido en conjunto', 'menu': menu}
            return render(request, 'app/paciente/buscarPaciente.html', context)


def listPaciente(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    paciente = Paciente.objects.all()
    context = {'pacientes': paciente,'menu':menu,'message':'Lista de pacientes' }

    return render(request, 'app/paciente/pacienteList.html', context)


def buscarPaciente(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    context = {'menu':menu}
    return render(request, 'app/paciente/buscarPaciente.html', context)

def savePaciente(request):
    flag = request.POST['flag']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    apellido_c = request.POST['apellidoC']
    telefono = request.POST['telefono']
    fecha_nacimiento = request.POST['fechaN']
    email = request.POST['email']
    direccion = request.POST['direccion']
    genero = request.POST['genero']

    nombreR = request.POST['nombreR']
    apellidoR = request.POST['apellidoR']
    telefonoR = request.POST['telefonoR']
    fecha_nacimientoR = request.POST['fechaNR']
    emailR = request.POST['emailR']
    direccionR = request.POST['direccionR']
    generoR = request.POST['generoR']


    # Verificamos si los datos que se enviaron son para edicion o para registro
    if flag == '1':
        # Codigo para editar
        paciente = Paciente.objects.get(id_paciente=request.POST['pid'])
        p = Persona.objects.get(id_persona=paciente.id_persona.id_persona)
        p.nombre = nombre
        p.apellido = apellido
        p.telefono = telefono
        p.apellido_casada = apellido_c
        p.email = email
        p.direccion = direccion
        p.genero = genero
        p.save()
        resp = Persona.objects.get(id_persona=paciente.per_id_persona)
        resp.nombre = nombreR
        resp.apellido = apellidoR
        resp.telefonos = telefonoR
        resp.fecha_nacimiento = fecha_nacimientoR
        resp.direccion = direccionR
        resp.genero = generoR
        resp.save()

    else:
        # Codigo si estamos registrando un nuevo paciente
        # Creamos un registro en persona para los datos del reponsable
        resp = Persona(nombre=nombreR, apellido=apellidoR, telefonos=telefonoR, fecha_nacimiento=fecha_nacimientoR,
                       email=emailR, direccion=direccionR, genero=generoR)
        resp.save()

        # Creamos un registro en persona para los datos del paciente
        p = Persona(nombre=nombre, apellido=apellido, telefonos=telefono, fecha_nacimiento=fecha_nacimiento,
                    apellido_casada=apellido_c,
                    email=email, direccion=direccion, genero=genero)
        p.save()

        # Creamos el paciente utlizando la relacion que tiene con persona y le mandamos los datos propios del paciente
        pac = p.paciente_set.create(per_id_persona=resp)

        parentesco = request.POST['sel1']
        f = Familiares(id_persona=resp, id_paciente=pac, parentesco=parentesco)
        f.save()

        # Guardamos el historial medico del paciente
        for i in range(1, 16): #segun codigo  de la tabla catalogo .. idcatalogo :1 , id catalogo :2
            enf = request.POST.get('en' + str(i), False)
            if enf:
                c = Catalogo.objects.get(id_catalogo=i)
                padecimiento = Padecimientos(id_paciente=pac, id_catalogo=c)
                padecimiento.save()



    return redirect('/app/paciente/listar')

def agregarPaciente(request):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    context = {'menu':menu}
    return render(request, 'app/paciente/agregarPaciente.html', context)


def parientesSave(request):
    flag = request.POST['flag']
    paciente = Paciente.objects.get(id_paciente=request.POST['paciente_id'])
    if flag == '1':
        p = Persona.objects.get(id_persona=request.POST['pariente'])
        p.nombre = request.POST['nombre']
        p.apellido = request.POST['apellido']
        p.telefonos = request.POST['telefono']
        p.email = request.POST['email']
        p.direccion = request.POST['direccion']
        p.genero = request.POST['genero']
        p.fecha_nacimiento = request.POST['fechaN']
        p.save()
        familiar = Familiares.objects.get(id_persona=p)
        familiar.parentesco = request.POST['sel1']
        familiar.save()

    else:
        p = Persona(nombre=request.POST['nombre'], apellido=request.POST['apellido'], telefonos=request.POST['telefono'], email=request.POST['email'], direccion= request.POST['direccion'], genero = request.POST['genero'],
                fecha_nacimiento=request.POST['fechaN'])
        p.save()

        f = Familiares(id_persona=p, id_paciente=paciente, parentesco=request.POST['sel1'])
        f.save()

    return redirect('/app/paciente/familiares/' + str(paciente.id_paciente))


def parientesIndex(request, paciente_id):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    familia = Familiares.objects.filter(id_paciente=paciente_id)
    paciente = Paciente.objects.get(id_paciente=paciente_id)
    context = {'familia': familia, 'paciente': paciente, 'menu': menu}

    return render(request, 'app/paciente/listarParientes.html', context)



def historialIndex(request, paciente_id):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    padecimientos = Padecimientos.objects.filter(id_paciente=paciente_id)
    catalogo = Catalogo.objects.filter(id_tipo_catalogo=1)
    paciente = Paciente.objects.get(id_paciente=paciente_id)

    context={'padecimientos':padecimientos, 'enfermedades':catalogo, 'paciente':paciente, 'menu':menu}
    return render(request, 'app/paciente/indexHistorial.html', context)



def historialUpdate(request):
    pacid = request.POST['pacid']
    paciente = Paciente.objects.get(id_paciente=pacid)
    for i in range (1,16):
        enf = request.POST.get('en'+str(i), False) #enfermedad no esta chequedada pone false
        if enf:
                pad = Padecimientos.objects.filter(id_paciente= pacid, id_catalogo=i).exists() #si esta cheuqeda verifica si enfermedad para ese paciente existe, existe devuelev true
                if not pad:
                    c = Catalogo.objects.get(id_catalogo=i) #entra si ese padecimiento no existe y recupera enfermedad
                    padecimiento = Padecimientos(id_catalogo=c, id_paciente=paciente) # crea padecimietno
                    padecimiento.save()
        else:
                pad = Padecimientos.objects.filter(id_paciente=pacid, id_catalogo=i)
                if pad.exists():
                   pad.delete()

    return redirect('/app/paciente/historial/'+ str(pacid))


def bitacoraIndex(request, serv_id):
    # menu
    perUser = Persona.objects.get(user=request.user)
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_group FROM auth_user WHERE id =  %s ", [perUser.user.id])
        row = cursor.fetchone()
    recu = GrupoRecurso.objects.filter(id_grupo=row)
    menu = Recurso.objects.filter(
        id_recurso__in=GrupoRecurso.objects.filter(id_grupo=row).values_list('id_recurso'))
    # menu
    registros = Bitacora.objects.filter(id_serviciohospitalario = serv_id)
    servicio = ServicioHospitalario.objects.get(id_servicio_hospitalario = serv_id)
    context = {'registros': registros, 'servicio': servicio,'menu':menu}

    return render(request, 'app/paciente/indexBitacora.html', context)

def bitacoraSave(request):
    serv_id = request.POST['serv_id']
    comentario = request.POST.get('comentario')
    fecha = datetime.datetime.now()

    servicio = ServicioHospitalario.objects.get(id_servicio_hospitalario=str(serv_id))

    bitacora = Bitacora(id_serviciohospitalario=servicio, comentario=comentario, fecha=fecha)
    bitacora.save()

    return redirect('/app/serviciohospitalario/bitacora/'+ str(serv_id))


def bitacoraUpdate(request):
    bitacora_id = request.POST['bitacora_id']
    bitacora = Bitacora.objects.get(id_bitacora=bitacora_id)
    comentario = request.POST.get('editc')
    bitacora.comentario = comentario
    bitacora.save()

    return redirect('/app/serviciohospitalario/bitacora/' + str(bitacora.id_serviciohospitalario.id_servicio_hospitalario))

