from django.contrib import admin

# Register your models here.
from .models import Adjuntos, Catalogo,\
    Cirugias, Cita, ConsultaMedica, Diagnosticos, Examen,\
    Familiares, IngresoHospitalario, Medico, Paciente,Padecimientos,Persona,ProgramacionCirugia,\
    Recurso,SeguimientoCirugia,ServicioHospitalario,SignosVitales,TipoCatalogo,Tratamientos


# Register your models here.
admin.site.register(Adjuntos)
admin.site.register(Catalogo)
admin.site.register(Cirugias)
admin.site.register(Cita)
admin.site.register(ConsultaMedica)
admin.site.register(Diagnosticos)
admin.site.register(Examen)
admin.site.register(Familiares)
admin.site.register(IngresoHospitalario)
admin.site.register(Medico)
admin.site.register(Paciente)
admin.site.register(Padecimientos)
admin.site.register(Persona)
admin.site.register(ProgramacionCirugia)
admin.site.register(Recurso)
admin.site.register(SeguimientoCirugia)
admin.site.register(ServicioHospitalario)
admin.site.register(SignosVitales)
admin.site.register(TipoCatalogo)
admin.site.register(Tratamientos)
