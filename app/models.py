# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User,Group
from django.db.models.signals import post_save
from django.dispatch import receiver




class Adjuntos(models.Model):
    id_adjunto = models.AutoField(db_column='ID_ADJUNTO', primary_key=True)  # Field name made lowercase.
    id_examen = models.ForeignKey('Examen', models.DO_NOTHING, db_column='ID_EXAMEN', blank=True, null=True)  # Field name made lowercase.
    imagen = models.FileField(upload_to='imagenes/%Y/%m/%d/',db_column='IMAGEN',blank=True, null=True,max_length=200)  # Field name made lowercase.
    video = models.FileField(upload_to='video/%Y/%m/%d/', db_column='VIDEO', blank=True, null=True, max_length=200)
    audio = models.FileField(upload_to='audio/%Y/%m/%d/', db_column='AUDIO', blank=True, null=True, max_length=200)
    def __str__(self):
        return '{}{}{}{}{}'.format(self.id_adjunto,self.id_examen,self.imagen,self.video,self.audio)
    class Meta:

        db_table = 'adjuntos'

class Bitacora(models.Model):
      id_bitacora = models.AutoField(db_column='ID_BITACORA', primary_key=True)
      id_serviciohospitalario = models.ForeignKey('ServicioHospitalario', models.DO_NOTHING, db_column='ID_SERVICIOHOSPITALARIO')
      comentario = models.CharField(db_column='COMENTARIO',max_length=255)
      fecha = models.DateTimeField(db_column='FECHA')

      class Meta:

          db_table = 'bitacora'



class Catalogo(models.Model):
    id_catalogo = models.AutoField(db_column='ID_CATALOGO', primary_key=True)  # Field name made lowercase.
    catalogo = models.CharField(db_column='CATALOGO', max_length=100)  # Field name made lowercase.
    codigo = models.CharField(db_column='CODIGO', max_length=20)  # Field name made lowercase.
    id_medico = models.ForeignKey('Medico', models.DO_NOTHING, db_column='ID_MEDICO', blank=True, null=True)  # Field name made lowercase.
    id_tipo_catalogo = models.ForeignKey('TipoCatalogo', models.DO_NOTHING, db_column='ID_TIPO_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.
    costo = models.FloatField(db_column='COSTO')  # Field name made lowercase.
    def __str__(self):
        return '{}'.format(self.catalogo)

    class Meta:

        db_table = 'catalogo'


class Cirugias(models.Model):
    id_cirugia2 = models.CharField(db_column='ID_CIRUGIA2', primary_key=True, max_length=10)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA')  # Field name made lowercase.
    procedimiento = models.CharField(db_column='PROCEDIMIENTO', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'cirugias'


class Cita(models.Model):
    fecha = models.DateField(db_column='FECHA')  # Field name made lowercase.
    fecha_registro = models.DateField(db_column='FECHA_REGISTRO')  # Field name made lowercase.
    id_cita = models.AutoField(db_column='ID_CITA', primary_key=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)  # Field name made lowercase.
    id_medico = models.ForeignKey('Medico', models.DO_NOTHING, db_column='ID_MEDICO', blank=True, null=True)  # Field name made lowercase.
    codigo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='CODIGO', blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'cita'


class ConsultaMedica(models.Model):
    fecha_consulta = models.DateField(db_column='FECHA_CONSULTA')  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.
    id_conultamedica = models.AutoField(db_column='ID_CONULTAMEDICA', primary_key=True)  # Field name made lowercase.
    id_medico = models.ForeignKey('Medico', models.DO_NOTHING, db_column='ID_MEDICO', blank=True, null=True)  # Field name made lowercase.
    id_signovital2 = models.OneToOneField('SignosVitales', models.DO_NOTHING, db_column='ID_SIGNOVITAL2', blank=True, null=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)  # Field name made lowercase.
    def __str__(self):
        return '{}{}{}{}{}{}'.format(self.fecha_consulta,self.descripcion,self.id_conultamedica,self.id_medico,self.id_signovital2,self.id_paciente)

    class Meta:

        db_table = 'consulta_medica'


class Diagnosticos(models.Model):
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)
    id_diagnostico2 = models.AutoField(db_column='ID_DIAGNOSTICO2', primary_key=True)  # Field name made lowercase.
    id_conultamedica = models.ForeignKey(ConsultaMedica,  db_column='ID_CONULTAMEDICA', blank=True, null=True,on_delete=models.CASCADE)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.
    fecha_servicio = models.DateField(db_column='FECHA_SERVICIO')  # Field name made lowercase.
    def __str__(self):
        return '{}{}{}{}{}'.format(self.id_diagnostico2,self.id_conultamedica,self.descripcion,self.fecha_servicio,self.id_catalogo)

    class Meta:

        db_table = 'diagnosticos'


class Examen(models.Model):
    id_examen = models.AutoField(db_column='ID_EXAMEN', primary_key=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA', blank=True, null=True)  #
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)
    id_conultamedica = models.ForeignKey(ConsultaMedica, models.DO_NOTHING, db_column='ID_CONULTAMEDICA', blank=True,null=True)
    exa_realizado = models.CharField(db_column='EXA_REALIZADO', max_length=1, blank=True, null=True,default='n')
    exa_revisado = models.CharField(db_column='EXA_REVISADO', max_length=1, blank=True, null=True,default='n')
    fecha_realizado= models.DateField(db_column='FECHA_REALIZADO', blank=True, null=True)
    def __str__(self):
        return '{}{}{}{}{}{}{}{}'.format(self.id_examen,self.id_catalogo,self.fecha,
                                   self.id_paciente,self.id_conultamedica,self.exa_realizado,self.exa_revisado,
                                   self.fecha_realizado)

    class Meta:

        db_table = 'examen'


class Familiares(models.Model):
    id_persona = models.ForeignKey('Persona', models.DO_NOTHING, db_column='ID_PERSONA', primary_key=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE')  # Field name made lowercase.
    parentesco = models.CharField(db_column='PARENTESCO', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'familiares'
        unique_together = (('id_persona', 'id_paciente'),)


class GrupoRecurso(models.Model):
    id_recurso = models.ForeignKey('Recurso', models.DO_NOTHING, db_column='ID_RECURSO')
    id_grupo = models.ForeignKey(Group, models.DO_NOTHING, db_column='ID_GRUPO')

    class Meta:
        db_table='gruporecurso'

class IngresoHospitalario(models.Model):
    id_ingresohospitalario2 = models.AutoField(db_column='ID_INGRESOHOSPITALARIO2', primary_key=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey('Paciente', models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)  # Field name made lowercase.
    cat_codigo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='CAT_CODIGO', blank=True, null=True, related_name='cat_codigo')  # Field name made lowercase.
    fecha_ingreso = models.DateField(db_column='FECHA_INGRESO')  # Field name made lowercase.
    numcuarto=models.IntegerField(db_column='NUMCUARTO',blank=True, null=True)
    numcamilla=models.IntegerField(db_column='NUMCAMILLA',blank=True, null=True)
    id_medico = models.ForeignKey('Medico', db_column='ID_MEDICO', on_delete=models.CASCADE, blank=True, null=True)
    medicamentos=models.CharField(db_column='MEDICAMENTOS',max_length=200, blank=True, null=True)
    alergias=models.CharField(db_column='ALERGIAS',max_length=200, blank=True, null=True)
    reanimacion=models.CharField(db_column='REANIMACION',max_length=100)
    activo=models.CharField(db_column='ACTIVO',max_length=1, blank=True, null=True)
    fecha_salida = models.DateField(db_column='FECHA_SALIDA', blank=True, null=True)
    condicion_salida = models.CharField(db_column='CONDICION_SALIDA', max_length=200, blank=True, null=True)
    tipo_alta = models.CharField(db_column='TIPO_ALTA', max_length=100, blank=True, null=True)
    cuidados = models.CharField(db_column='CUIDADOS', max_length=200, blank=True, null=True)
    def __str__(self):
        return '{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}'.format(self.id_ingresohospitalario2,self.id_catalogo,self.id_paciente,self.cat_codigo
                                     ,self.fecha_ingreso,self.numcuarto,self.numcamilla,self.id_medico,self.medicamentos,
                                     self.alergias,self.reanimacion,self.activo,self.fecha_salida,self.condicion_salida,
                                                 self.tipo_alta,self.cuidados)


    class Meta:

        db_table = 'ingreso_hospitalario'


class Medico(models.Model):
    id_medico = models.IntegerField(db_column='ID_MEDICO', primary_key=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    id_ingresohospitalario2 = models.ForeignKey(IngresoHospitalario, models.DO_NOTHING, db_column='ID_INGRESOHOSPITALARIO2', blank=True, null=True)  # Field name made lowercase.
    id_persona = models.ForeignKey('Persona', models.DO_NOTHING, db_column='ID_PERSONA', blank=True, null=True)  # Field name made lowercase.
    codigo_medico = models.CharField(db_column='CODIGO_MEDICO', max_length=15)  # Field name made lowercase.

    def __str__(self):
        return '{}'.format(self.id_medico)

    class Meta:

        db_table = 'medico'


class Paciente(models.Model):
    id_paciente = models.AutoField(db_column='ID_PACIENTE', primary_key=True)  # Field name made lowercase.
    id_persona = models.ForeignKey('Persona', models.DO_NOTHING, db_column='ID_PERSONA', blank=True, null=True)  # Field name made lowercase.
    codigo_expediente = models.CharField(db_column='CODIGO_EXPEDIENTE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    per_id_persona = models.ForeignKey('Persona', models.DO_NOTHING, blank=True, null=True,
                                       related_name='per_id_persona')
    def __str__(self):
        return '{}{}'.format(self.id_paciente, self.id_persona)

    class Meta:

        db_table = 'paciente'




class Padecimientos(models.Model):
    id_padecimiento = models.AutoField(db_column="ID_PADECIMIENTO", primary_key=True)
    id_paciente = models.ForeignKey(Paciente, models.DO_NOTHING, db_column='ID_PACIENTE')  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO')  # Field name made lowercase.
    activo = models.IntegerField(db_column='ACTIVO', blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'padecimientos'
        unique_together = (('id_paciente', 'id_catalogo'),)


class Persona(models.Model):
    apellido = models.CharField(db_column='APELLIDO', max_length=100)
    fecha_nacimiento = models.DateField(db_column='FECHA_NACIMIENTO')
    direccion = models.CharField(db_column='DIRECCION', max_length=200)
    genero = models.CharField(db_column='GENERO', max_length=1)
    telefonos = models.CharField(db_column='TELEFONOS', max_length=10)
    email = models.CharField(db_column='EMAIL', max_length=50, blank=True, null=True)
    nombre = models.CharField(db_column='NOMBRE', max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    apellido_casada = models.CharField(db_column='APELLIDO_CASADA', max_length=100, blank=True, null=True)
    id_persona = models.AutoField(db_column='ID_PERSONA', primary_key=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return '{}'.format(self.nombre,self.apellido)
    class Meta:

        db_table = 'persona'


class ProgramacionCirugia(models.Model):
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    id_conultamedica = models.ForeignKey(ConsultaMedica,  db_column='ID_CONULTAMEDICA', primary_key=True,on_delete=models.CASCADE)  # Field name made lowercase.
    fecha_programada = models.DateField(db_column='FECHA_PROGRAMADA', blank=True, null=True)  # Field name made lowercase.
    hora = models.CharField(db_column='HORA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    def __str__(self):
        return '{}{}{}{}'.format(self.id_catalogo,self.id_conultamedica,self.fecha_programada,self.hora)

    class Meta:

        db_table = 'programacion_cirugia'


class Recurso(models.Model):
    id_recurso = models.IntegerField(db_column='ID_RECURSO', primary_key=True)  # Field name made lowercase.
    recurso_padre = models.ForeignKey('self',db_column='recurso_id' , on_delete=models.CASCADE,null=True)
    nombre=models.CharField(db_column='NOMBRE', max_length=100, blank=True, null=True)
    icono = models.CharField(db_column='ICONO', max_length=50, blank=True, null=True)
    url = models.CharField(db_column='URL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    es_detalle = models.CharField(db_column='ES_DETALLE', max_length=24, blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'recurso'

class Resultados(models.Model):
    id_resultado= models.AutoField(db_column='ID_RESULTADO', primary_key=True)
    encargado=models.CharField(db_column='ENCARGADO',max_length=200,null=True,blank=True)
    id_examen=models.ForeignKey(Examen,db_column='ID_EXAMEN', on_delete=models.CASCADE,null=True)
    descripcion= models.CharField(db_column='DESCRIPCION',max_length=300, blank=True,null=True)
    resultado=models.CharField(db_column='RESULTADO',max_length=300,blank=True,null=True)
    observacion=models.CharField(db_column='OBSERVACION',max_length=300,blank=True,null=True)

    def __str__(self):
        return '{}{}{}{}{}{}'.format(self.id_resultado, self.encargado, self.id_examen,self.descripcion,
                                       self.resultado, self.observacion)

    class Meta:
        db_table='RESULTADOS'

class SeguimientoCirugia(models.Model):
    id_postoperacion2 = models.AutoField(db_column='ID_POSTOPERACION2', primary_key=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    id_cirugia2 = models.ForeignKey(Cirugias, models.DO_NOTHING, db_column='ID_CIRUGIA2', blank=True, null=True)  # Field name made lowercase.
    fecha_servicio = models.DateField(db_column='FECHA_SERVICIO')  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.

    class Meta:

        db_table = 'seguimiento_cirugia'


class ServicioHospitalario(models.Model):
    id_servicio_hospitalario = models.AutoField(db_column='ID_SERVICIO_HOSPITALARIO', primary_key=True)  # Field name made lowercase.
    id_medico=models.ForeignKey(Medico,db_column='ID_MEDICO', blank=True, null=True, on_delete=models.CASCADE)
    id_ingresohospitalario2 = models.ForeignKey(IngresoHospitalario, models.DO_NOTHING, db_column='ID_INGRESOHOSPITALARIO2', blank=True, null=True)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    fecha_inicio = models.DateField(db_column='FECHA_INICIO')  # Field name made lowercase.
    duracion=models.CharField(db_column='DURACION',max_length=100, blank=True, null=True)
    dosis=models.CharField(db_column='DOSIS',max_length=100, blank=True, null=True)
    def __str__(self):
        return '{}{}{}{}{}{}{}'.format(self.id_persona,self.id_ingresohospitalario2,self.id_catalogo,self.fecha_inicio,
                                 self.id_servicio_hospitalario,self.duracion,self.dosis)

    class Meta:

        db_table = 'servicio_hospitalario'


class SignosVitales(models.Model):
    id_signovital2 = models.AutoField(db_column='ID_SIGNOVITAL2', primary_key=True)  # Field name made lowercase.
    id_paciente = models.ForeignKey(Paciente, models.DO_NOTHING, db_column='ID_PACIENTE', blank=True, null=True)  # Field name made lowercase.
    peso = models.FloatField(db_column='PESO')  # Field name made lowercase.
    estatura = models.FloatField(db_column='ESTATURA')  # Field name made lowercase.
    presion_arterial = models.CharField(db_column='PRESION_ARTERIAL', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ritmo_cardiaco = models.CharField(db_column='RITMO_CARDIACO', max_length=20, blank=True, null=True)  # Field name made lowercase.
    frecuencia_respiratoria = models.CharField(db_column='FRECUENCIA_RESPIRATORIA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA', blank=True, null=True)  # Field name made lowercase.
    med = models.CharField(db_column='MED', max_length=1, blank=True, null=True, default='n')
    def __str__(self):
        return '{}{}{}{}{}{}{}{}'.format (self.id_signovital2, self.id_paciente,self.peso,self.estatura,self.presion_arterial,self.ritmo_cardiaco,self.frecuencia_respiratoria,self.fecha)

    class Meta:

        db_table = 'signos_vitales'


class TipoCatalogo(models.Model):
    id_tipo_catalogo = models.IntegerField(db_column='ID_TIPO_CATALOGO', primary_key=True)  # Field name made lowercase.
    tipo_catalogo = models.CharField(db_column='TIPO_CATALOGO', max_length=100)  # Field name made lowercase.

    def __str__(self):
        return '{}'.format(self.id_tipo_catalogo,self.tipo_catalogo)

    class Meta:

        db_table = 'tipo_catalogo'


class Tratamientos(models.Model):
    id_tratamiento2 = models.AutoField(db_column='ID_TRATAMIENTO2', primary_key=True)  # Field name made lowercase.
    id_conultamedica = models.ForeignKey(ConsultaMedica, db_column='ID_CONULTAMEDICA', blank=True, null=True,on_delete=models.CASCADE)  # Field name made lowercase.
    id_catalogo = models.ForeignKey(Catalogo, models.DO_NOTHING, db_column='ID_CATALOGO', blank=True, null=True)  # Field name made lowercase.
    fecha_servicio = models.DateField(db_column='FECHA_SERVICIO')  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=200)  # Field name made lowercase.
    dosis = models.CharField(db_column='DOSIS', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:

        db_table = 'tratamientos'